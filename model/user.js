const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const UserSchema = new Schema({
  name: String,
  race: String,
  family: String,
  food: String,
  age: Number,
  password: String,
  friends: Array
});

const User = module.exports = mongoose.model('User', UserSchema)
