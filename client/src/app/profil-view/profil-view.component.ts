import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { User } from '../user';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-profil-view',
  templateUrl: './profil-view.component.html',
  styleUrls: ['./profil-view.component.scss']
})
export class ProfilViewComponent implements OnInit {

  user: User;

  id = '';
  name = '';
  age = undefined;
  family = '';
  race = '';
  food = '';
  friends = [];
  allUsers = [];

  isEditing = false;

  constructor(public authService: AuthenticationService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.id = localStorage.getItem('token')

    if(!this.id) this.router.navigate(['/'])

    // get the user's information
    this.authService.getUserById(this.id)
      .subscribe(res => {
        if(!res['response']) this.router.navigate(['/'])

        this.initUser(res['user'])
      })
    // get the other users
    this.authService.getAllUsers().subscribe(res => {
      res['users'] = res['users'].filter(user => user.name !== this.name)
      this.allUsers = res['users']
      // remove users already friends
      if(this.friends) this.allUsers = this.allUsers.filter(user => !this.friends.find(friend => friend.name == user.name))
    })
  }

  initUser(user) {
    this.user = new User(user)
    this.name = user.name
    this.age = user.age
    this.family = user.family
    this.race = user.race
    this.food = user.food
    this.friends = user.friends
  }

  onEditProfil = () => {
    this.isEditing = true
  }

  onSaveProfil = () => {
    //
    const newUser = new User()
    newUser.name = this.name
    newUser.age = this.age
    newUser.id = this.id
    newUser.family = this.family
    newUser.race = this.race
    newUser.food = this.food
    newUser.friends = this.friends

    this.authService.updateUser(newUser)
      .subscribe(res => {
        if(res['response']) {
          this.isEditing = false
          this.user.name = this.name
          this.user.age = this.age
          this.user.family = this.family
          this.user.race = this.race
          this.user.food = this.food
          this.user.friends = this.friends
        } else {
          alert('Problem...')
        }
      })
  }

  onAddFriend = (friend) => {
    // add friend to update database
    if(this.friends){
      this.friends.push(friend)
    } else {
      this.friends = [friend]
    }

    this.user.friends = this.friends

    this.authService.updateUser(this.user)
      .subscribe(res => {
        if(res['response']) {
          alert('Friend added')
          const index = this.allUsers.indexOf(friend)
          if(index != -1 && this.allUsers.length == 1) this.allUsers = []
          else this.allUsers = this.allUsers.slice(0, index).concat(this.allUsers.slice(index + 1, this.allUsers.length))
        } else {
          alert('Problem...')
        }
      })
  }

  onDeleteFriend = (friend) => {
    // remove friend from the list
    this.friends = this.friends.filter(user => user.name !== friend.name)
    this.user.friends = this.friends

    this.authService.updateUser(this.user)
      .subscribe(res => {
        if(res['response']) {
          alert('Friend removed')
          const user = res['user']
          this.allUsers = this.allUsers.concat(friend)
        } else {
          alert('Problem...')
        }
      })
  }
}
