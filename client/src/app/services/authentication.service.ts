import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

import { User } from '../user';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient) { }

  logout() :void {
     localStorage.setItem('isLoggedIn','false');
     localStorage.removeItem('token');
  }

  getId() {
    return localStorage.getItem('token');
  }

  login(id) {
    localStorage.setItem('isLoggedIn', "true");
    localStorage.setItem('token', id);
  }

  isLoggedIn(): boolean {
    return localStorage.getItem('isLoggedIn') == 'true';
  }

  getUserById(id: string) {
    return this.http.get('http://localhost:3000/api/user/' + id)
  }

  getUser(user: User) {
    return this.http.post<User>('http://localhost:3000/api/user/identify', user)
    .pipe(
      catchError(this.handleError)
    )
  }

  createUser(user: User) {
    return this.http.post<User>('http://localhost:3000/api/user', user)
    .pipe(
      catchError(this.handleError)
    )
  }

  getAllUsers() {
    return this.http.get('http://localhost:3000/api/user/')
  }

  updateUser(user: User) {
    return this.http.put<User>('http://localhost:3000/api/user/', user)
    .pipe(
      catchError(this.handleError)
    )
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }
}
