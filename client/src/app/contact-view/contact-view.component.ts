import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { User } from '../user';

@Component({
  selector: 'app-contact-view',
  templateUrl: './contact-view.component.html',
  styleUrls: ['./contact-view.component.scss']
})
export class ContactViewComponent implements OnInit {

  _name: string;
  user: User;
  canAdd = false;
  constructor(public authService: AuthenticationService) { }

  ngOnInit(): void {
    this.authService.getUserById(this.authService.getId())
      .subscribe(res => {
        this.user = new User(res['user'])
      })
  }

  onUpdateInput(att, value) {
    this[att] = value;
    if(value.length>0) this.canAdd = true;
    else this.canAdd = false;
  }

  onAdd() {
    const newUser = new User()
    newUser.name = this._name
    // create contact
    this.authService.createUser(newUser).subscribe(res => {
      if(res['id']) {
        alert('Contact ajouté')
        // ajouter friend
        this.addAsFriend(res['user'])
      }
      else if (res['alreadyExist']) {
        alert('This name is already used')
      }
      else alert('Error creation')
    })
  }

  addAsFriend(newFriend) {
    // add newFriend
    if(this.user.friends) {
      this.user.friends.push(newFriend)
    } else {
      this.user.friends = [newFriend]
    }

    // update user
    this.authService.updateUser(this.user)
      .subscribe(res => {
        if(res['response']) { // success
          alert('Nouvel ami')
        } else { // error
          alert('Problem...')
        }
      })
  }
}
