import { Component } from '@angular/core';
import { AuthenticationService } from './services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'adopteunpangolin.com';
  isConnected = this.authService.isLoggedIn()

  constructor(public authService: AuthenticationService, private router: Router) { }

  logout(event) {
    this.authService.logout();
    this.router.navigate(['/']);
  }
}
