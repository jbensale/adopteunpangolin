import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { User } from '../user';

@Component({
  selector: 'app-authentication-view',
  templateUrl: './authentication-view.component.html',
  styleUrls: ['./authentication-view.component.scss']
})
export class AuthenticationViewComponent implements OnInit {

  pseudoLogin: string = '';
  passwordLogin: string = '';
  pseudoCreate: string = '';
  passwordCreate: string = '';
  passwordBisCreate: string = '';

  canLogin = false;
  canCreate = false;

  constructor(
    public authService: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute
  )
    { }

  ngOnInit(): void {
    this.authService.logout();
  }

  onUpdateInput(attribute: string, value: string) {
    this[attribute] = value;
    this.canLogin = this.pseudoLogin.length > 0 && this.passwordLogin.length > 0
    this.canCreate = this.pseudoCreate.length > 0 && this.passwordCreate.length > 0 && this.passwordBisCreate.length > 0
  }

  // log with existing account
  onConnect(event) {
    if(this.canLogin) {
      const user = new User()
      user.name = this.pseudoLogin
      user.password = this.passwordLogin
      this.authService.getUser(user).subscribe(res => {
        if(res['response']) {
            this.authService.login(res.id)
            window.location.href = '/profil'
        }
        else alert('No account')
      })
    } else {
      alert('Impossible to log in')
    }
  }

  // create account
  onCreate(event) {
    if(this.canCreate && this.passwordCreate === this.passwordBisCreate) {
      const user = new User()
      user.name = this.pseudoCreate
      user.password = this.passwordCreate
      this.authService.createUser(user).subscribe(res => {
        if(res['id']) {
          this.authService.login(res.id)
          window.location.href = '/profil'
        }
        else if (res['alreadyExist']) {
          alert('This name is already used')
        }
        else alert('Error creation')
      })
    } else {
      alert('Invalid')
    }
  }

}
