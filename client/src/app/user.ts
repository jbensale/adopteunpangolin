export class User {
  id: string;
  name: string;
  race: string;
  family: string;
  food: string;
  friends: Array<Object>;
  password: string;
  age: number;

  constructor(user = undefined) {
    if(user) {
      this.name = user.name
      this.id = user._id
      this.race = user.race
      this.family = user.family
      this.food = user.food
      this.friends = user.friends
      this.age = user.age
      this.password = user.password
    }
  }
}
