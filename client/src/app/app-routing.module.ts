import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfilViewComponent } from './profil-view/profil-view.component';
import { AuthenticationViewComponent } from './authentication-view/authentication-view.component';
import { HomeViewComponent } from './home-view/home-view.component';
import { ContactViewComponent } from './contact-view/contact-view.component';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  { path: 'profil', component: ProfilViewComponent, canActivate : [AuthGuard] },
  { path: 'authentication', component: AuthenticationViewComponent},
  { path: 'contact', component: ContactViewComponent, canActivate : [AuthGuard]},
  { path: '**', component: HomeViewComponent },
];

@NgModule({
imports: [RouterModule.forRoot(routes)],
exports: [RouterModule]
})
export class AppRoutingModule { }
