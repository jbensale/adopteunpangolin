const express = require('express')
const router = express.Router()

const User = require('../model/user')

// return user
router.get('/user/:id', (req, res, next) => {
  const id = req.params.id

  User.findById(id, (err, user) => {
    if(err) {
      res.json({message: 'Error', response: false})
    }
    else {
      if(user) res.json({response: true, user: user}) // the user exists
      else res.json({response: false})
    }
  })
})
// return all the users
.get('/user/', (req, res, next) => {
  User.find({}, (err, users) => {
    if(err) {
      res.json({message: 'Error', error: err})
    }
    else {
      res.json({message: 'Ok', response: true, users: users})
    }
  })
})
// identify user
.post('/user/identify', (req, res, next) => {
  if(req.body) {
    const {name, password} = req.body

    User.findOne({name: name}, (err, user) => {
      if(err) {
        res.json({message: 'Error', result: false})
      }
      else if(user) {
        res.json({message: 'Ok', response: user.password == password, id: user._id})
      } else res.json({message: 'No user', result: false})
    })
  }
})
// create new user
.post('/user', (req, res, next) => {

  if(!req.body || !req.body.name) {
    // credentials missing
    res.json({response: false,message: 'Incorrect information'})
  } else {

    let { name, password } = req.body
    User.findOne({ name: name }, (err, user) => {
      // already exist
      if(user) {
        res.json({alreadyExist: true, response: false})
      // new user
      } else if(name) {
        if(!password) password = '0000' // generate default password if new contact
        let newUser = new User({
          name,
          password
        })

        newUser.save((err, user) => {
          if(err) {
            res.json({response: false, message: 'Failed to create user', error: err})
          } else {
            res.json({response: true, message: 'User added', id: user._id, user: newUser})
          }
        })
      }
    })
  }
})
// update user
.put('/user', (req, res, next) => {
  const {name, age, friends, family, food, race, id} = req.body

  User.findByIdAndUpdate(
    id,
    { name, age, friends, family, food, race },
    { useFindAndModify: false, new: true },
    (err, user) => {
      if(err) {
        res.json({message: 'Error', error: err})
      } else {
        res.json({message: 'Ok', response: true, user: user})
      }
  })
})

module.exports = router
