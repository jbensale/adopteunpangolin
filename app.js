const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const path = require('path')
const mongoose = require('mongoose')

const app = express()

const uri = "mongodb+srv://maitre_pangolin:0NtT54FKu2ScETKE@cluster0-axc7y.gcp.mongodb.net/client?retryWrites=true&w=majority"
const port = 3000

const route = require('./routes/route')

mongoose.connect(uri, { useUnifiedTopology: true, useNewUrlParser: true })
mongoose.connection.on('connected', () => {
  console.log('Connected to Database')
})
mongoose.connection.on('error', (err) => {
  if(err) {
    console.log('Error with database connection', err)
  }
})

app.use(cors())
app.use(express.static(path.join(__dirname, 'public')))

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.use('/api', route)

app.get('*', (req, res) => {
  res.sendFile(__dirname + '/public/index.html')
})

app.listen(port, function() {
  console.log('Server running on port ' + port)
})
